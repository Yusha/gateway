
const express = require('express');
const bodyParser = require('body-parser');

const homeRouter = require('./routes/HomeRouter');
const objectRouter = require('./routes/ObjectRouter');
const messageRouter = require('./routes/MessageRouter');
const documentRouter = require('./routes/DocumentRouter');
const documentXchangeRouter = require ('./routes/DocumentXchangeRouter');
const apiRouter = require ('./routes/APIRouter');
const cTemplateRouter = require('./routes/CTemplateRouter');
const contractCodeRouter = require ('./routes/ContractCodeRouter');
const contractsRouter = require ('./routes/ContractsRouter');
const assetProofRouter = require ('./routes/AssetProofRouter');
const documentContractRouter = require('./routes/DocumentContractRouter');
const PIRouter = require('./routes/PIRouter');
const PIActionRouter = require('./routes/PIActionRouter');
const ConnectionRouter = require('./routes/ConnRouter');
const HomeInviteRouter = require('./routes/HomeInviteRouter');
const ActionRouter = require('./routes/ActionRouter');
const CTControlRouter = require('./routes/CTControlRouter');


const jobs = require('./jobs/ProducerJob');



const app = express();

app.use(express.json({limit: '10mb', extended: true}));

app.use(express.urlencoded({limit: "10mb", extended: true, parameterLimit:50000}));

app.use(bodyParser.json());


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


app.use('/home',homeRouter);
app.use('/object', objectRouter);
app.use('/message',messageRouter);
app.use('/documents', documentRouter);
app.use('/contracts/templates',cTemplateRouter);
app.use('/contracts/templates/controls',CTControlRouter);
app.use('/contracts/codes',contractCodeRouter);
app.use('/contracts/contracts',contractsRouter);
app.use('/cryptoxpress', documentXchangeRouter);

app.use('/fakeproof/assets', assetProofRouter);
app.use('/documentcontract',documentContractRouter);
app.use('/PI',PIRouter);
app.use('/PI/task',PIActionRouter);

app.use('/api',apiRouter);
app.use('/network',ConnectionRouter);
app.use('/homeInvite',HomeInviteRouter);
app.use('/actions',ActionRouter);



app.get('/', function(req, res) {
  res.send('Got it!');
});

jobs.startProducers();

app.listen(4000, function() {
  console.log('Gateway is listening on port: 4000!');
});
