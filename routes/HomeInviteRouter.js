const express = require('express');
const router = express.Router();


const hinvite_controller = require('../controllers/HomeInviteController');

// GET

router.get('/', hinvite_controller.index);

router.get('/get', hinvite_controller.get_invite);


// POST
router.post('/add', hinvite_controller.add_invite);

router.post('/exists', hinvite_controller.exists);


module.exports = router;