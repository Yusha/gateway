const express = require('express');
const router = express.Router();


const document_controller = require('../controllers/DocumentController');


router.get('/', document_controller.index);


router.get('/get', document_controller.get_documents);
router.post('/add', document_controller.add_document);



module.exports = router;
