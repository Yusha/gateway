const express = require('express');
const router = express.Router();


const conn_controller = require('../controllers/ConnController');

// GET

router.get('/', conn_controller.index);

router.get('/get', conn_controller.get_connections);


// POST
router.post('/add', conn_controller.add_connection);


module.exports = router;