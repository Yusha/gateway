var express = require('express');
var router = express.Router();


var asset_controller = require('../controllers/AssetProofController');


router.get('/', asset_controller.index);

// http://localhost:4000/fakeproof/asset/get?homeId=Microsoft

router.get('/get', asset_controller.get);

router.get('/getAsset', asset_controller.getAsset);

router.post('/add', asset_controller.add);


module.exports = router;
