var express = require('express');
var router = express.Router();


var home_controller = require('../controllers/Home');


router.get('/', home_controller.index);


router.post('/createHome', home_controller.create_home);


router.post('/createPhone', home_controller.create_home_phone);

router.post('/retirePhone', home_controller.retire_home_phone);


router.post('/createEmail', home_controller.create_home_email);

router.post('/retireEmail', home_controller.retire_home_email);


router.post('/createAdminAccount', home_controller.create_adminAccount);

router.post('/retireAdminAccount', home_controller.retire_adminAccount);


router.post('/createAccount', home_controller.create_account);

router.post('/retireAccount', home_controller.retire_account);

router.post('/createAddress', home_controller.create_address);

router.post('/retireAddress', home_controller.retire_address);

router.post('/createGroup', home_controller.create_group);
router.post('/retireGroup', home_controller.retire_group);

router.post('/createRole', home_controller.create_role);

router.post('/retireRole', home_controller.retire_role);

router.post('/invite', home_controller.invite);
router.post('/approveInvite', home_controller.approve_invite);
router.post('/validateInvite', home_controller.validate_invite);

router.post('/claimInvite', home_controller.claim_invite);
router.post('/inviteExists', home_controller.invitationExists);

router.get('/home', home_controller.get_home_details);

router.get('/status',home_controller.get_status);


router.get('/groups',home_controller.get_groups);


router.get('/roles',home_controller.get_roles);


router.get('/accounts',home_controller.get_accounts);
router.get('/account',home_controller.get_account);


router.get('/pendingInvitations',home_controller.get_pendingInvitations);

router.get('/approvedInvitations',home_controller.get_approvedInvitations);





module.exports = router;
