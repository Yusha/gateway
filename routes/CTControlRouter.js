var express = require('express');
var router = express.Router();


var ctemplate_controller = require('../controllers/CTControlController');


router.get('/', ctemplate_controller.index);

// http://localhost:4000/contracts/templates/controls/get?publisher=System

router.get('/get', ctemplate_controller.get);

router.post('/add', ctemplate_controller.add);

module.exports = router;
