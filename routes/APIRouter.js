var express = require('express');
var router = express.Router();


var api_controller = require('../controllers/APIController');


// GET APIs


router.get('/', api_controller.index);

router.get('/search',api_controller.searchHome);

router.get('/accounts',api_controller.getAccounts);

router.get('/account', api_controller.getAccountInfo);

router.get('/exists', api_controller.isExists);


// POST APIs

module.exports = router;
