var express = require('express');
var router = express.Router();


var dc_controller = require('../controllers/DocumentContractController');


router.get('/', dc_controller.index);

// http://localhost:4000/documentcontract/get?originator=Microsoft

router.get('/get', dc_controller.get);


router.post('/add', dc_controller.add);



module.exports = router;
