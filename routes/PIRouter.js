const express = require('express');
const router = express.Router();


const PI_controller = require('../controllers/PIController');

// GET

router.get('/', PI_controller.index);

router.get('/get', PI_controller.get_PI);
router.get('/PIO', PI_controller.get_PIO);


// POST
router.post('/add', PI_controller.add_PI);
router.post('/deactivate', PI_controller.deactivate_PIO);


module.exports = router;