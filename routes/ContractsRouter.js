var express = require('express');
var router = express.Router();


var contracts_controller = require('../controllers/ContractsController');


router.get('/', contracts_controller.index);

// http://localhost:4000/contracts/contracts/get?originator=Microsoft

router.get('/get', contracts_controller.get);

router.get('/contractdata', contracts_controller.getContractData);

router.get('/contract',contracts_controller.getContract);

router.get('/getSignatures',contracts_controller.getSignatures);

router.get('/getSignature',contracts_controller.getSignature);

router.get('/getContractsForType',contracts_controller.getContractForType);


// POST

router.post('/add', contracts_controller.add);
router.post('/sign',contracts_controller.sign);


module.exports = router;
