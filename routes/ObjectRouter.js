var express = require('express');
var router = express.Router();


var object_controller = require('../controllers/RO');


router.get('/', object_controller.index);


router.post('/createObject', object_controller.create_object);
router.post('/updateObject', object_controller.updateObject);

router.post('/retireObject', object_controller.retireObject);

router.post('/share', object_controller.shareObject);


// http://localhost:4000/object/getObjects?home=Microsoft
router.get('/getObjects', object_controller.get_objects);
router.get('/shared/get', object_controller.get_shared);


module.exports = router;
