const express = require('express');
const router = express.Router();


const PIAction_controller = require('../controllers/PIActionController');

router.get('/', PIAction_controller.index);

router.get('/get', PIAction_controller.get_PI);
router.post('/approve', PIAction_controller.approve_PI);


module.exports = router;