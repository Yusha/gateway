var express = require('express');
var router = express.Router();


var message_controller = require('../controllers/Message');


router.get('/', message_controller.index);


router.post('/sendMessage', message_controller.send_message);


router.get('/getMessages', message_controller.get_messages);

router.get('/inbox', message_controller.in_messages);

router.get('/sentbox', message_controller.sent_messages);



module.exports = router;