var express = require('express');
var router = express.Router();


var CCode_controller = require('../controllers/CCodeController');


router.get('/', CCode_controller.index);

// http://localhost:4000/contracts/codes/get?publisher=System

router.get('/get', CCode_controller.get);

router.post('/add', CCode_controller.add);

module.exports = router;
