const express = require('express');
const router = express.Router();


const Action_controller = require('../controllers/ActionController');

router.get('/', Action_controller.index);

router.get('/SignActions', Action_controller.get_SignTasks);


module.exports = router;