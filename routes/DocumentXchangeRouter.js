const express = require('express');
const router = express.Router();


const documentXchange_controller = require('../controllers/DocumentXchangeController');


router.get('/', documentXchange_controller.index);


router.get('/sentbox', documentXchange_controller.get_sentbox);
router.get('/inbox', documentXchange_controller.get_inbox);
router.get('/history',documentXchange_controller.get_history);

router.post('/send', documentXchange_controller.send_document);



module.exports = router;
