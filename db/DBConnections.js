// Initialise connection


let mongoose = require('mongoose');

let mongoDB = 'mongodb://127.0.0.1/home';

mongoose.connect(mongoDB, { useNewUrlParser: true });

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));

module.exports = db;

