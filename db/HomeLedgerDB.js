
var HomeLedger = require('../models/HomeLedger');

let mongoose = require('mongoose');

let mongoDB = 'mongodb://127.0.0.1/home';

mongoose.connect(mongoDB, { useNewUrlParser: true });

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));



exports.getLedgerDB = function() {
  return HomeLedger;
}


exports.produceBlock = async function (blockId,hash,previousHash,producer,producerSignature,ntx,txs) {
  
  try {
  
    var block = new HomeLedger({
      _id: blockId, hash: hash, previousBlockHash: previousHash,
      blockProducer: producer, blockProducerSign: producerSignature, numberOfTx: ntx, txs: txs
    });
    
    var result = await block.save();
    
    console.log("Block saved  " + JSON.stringify(result));
    return result;
    
  } catch (error ) {
    console.log("Error saving block  " + JSON.stringify(error));
    
  }
  
};