let mongoose = require('mongoose');

let HomeTx = require("../models/HomeTxPool");

let mongoDB = 'mongodb://127.0.0.1/home';

mongoose.connect(mongoDB, { useNewUrlParser: true });
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));


exports.getHomeTxObject = function() {
  return HomeTx;
}

exports.addTx = async function (hash,home,tx) {
  
  const newTx = new HomeTx({_id:hash,txHash:hash,homeId:home,ledgerId:0,tx:tx});
  
  
  try {
    
    const result = await newTx.save();
    // Add an entry in Receipt pool as well...
    
    console.log("Success saving..Result is  " + JSON.stringify(result));
    return result;
  } catch (error) {
    console.log('Error saving the tx  ' + error);
    return "Error in saving tx ... ";
  }
};

exports.homeExists = async function (home) {
  
  try {
  
    const result = await homeTx.fineOne({homeId: home});
    
    console.log("Result if Exists is " + JSON.stringify(result));
    
    return result;
  } catch (error) {
    console.log('Error in homeExist  ' + error);
    return "Error in homeExist ... ";
    
  }

};


exports.getBatchTxs =  async function (size) {
  try {
    
    const result =  await homeTx.find().limit(size);
    rString = JSON.stringify(result);
    console.log(rString);
    
    return rString;
      
    
    
  } catch (error) {
    return "Error ... "+ error;
  }
  
};
