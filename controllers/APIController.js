

const ObjectTxPool = require('../models/ObjectTxPool');
const HomeCache = require('../models/HomeCache');
const PI = require('../models/PI');

const db = require('../db/DBConnections');


// GET CALLS

exports.index = function(req, res) {
  res.send('NOT IMPLEMENTED: Home Ledger. Index route for testing');
};



exports.searchHome = function (req,res) {
  let homeId = req.query.home;
  
  let found = false;
  
  const toSend = {
    profile: {},
    contacts: [],
    objects: [],
    PIs: []
  };
  
  // Todo: Remove the retired or cancelled contacts...
  
  const homePromise = HomeCache.find({_id: homeId}).then(result => {
    if (result.length > 0) {
  
      const retired = [];
      const all = JSON.parse(JSON.stringify(result[0].txs));
      const gtx = JSON.parse(all[0].tx);
      const itx = JSON.parse(gtx.tx.txs);
  
      toSend.profile.homeId = itx.homeId;
      toSend.profile.businessName = itx.businessName;
  
      toSend.profile.address = itx.addresses.address + ", " + itx.addresses.country;
      toSend.profile.phone = itx.phones.number;
      toSend.profile.email = itx.emails.email;
      found = true;
      
      const reversed = all.reverse();
  
  
      reversed.forEach((tx, index) => {
        const txo = JSON.parse(tx.tx);
        
        if (txo.tx.action ==="newHome") {
        
          const txs = JSON.parse(txo.tx.txs);
          const adminAccount = txs.adminAccounts;
          
          toSend.contacts.push({
            name: adminAccount.name,
    
            email: adminAccount.email,
            phone: adminAccount.phone,
            group: "superAdmin",
            role: "admin",
            approvee:"genesis",
            publicKey: adminAccount.publicKey
          });
          return;
        }
    
        const insideTx = txo.tx.tx;
        
        if (!insideTx) return;
        
        const txactual = JSON.parse(insideTx);
    
        if (txo.tx.action === "retireAccount") {
          retired.push(txo.tx.account);
        }
        if (txo.tx.action === "newAccount") {
          
          if (retired.includes(txo.tx.owner)) {
            console.log("found retired " + txo.tx.owner);
          } else {
            
            const invitedTxs = txo.tx.invitedTxs;
            if (!invitedTxs) {
              return;
            }
  
            const invitedTx = JSON.parse(txo.tx.invitedTxs);
            
            
            toSend.contacts.push({
              name: txactual.name,
              
              email: txo.tx.owner,
              phone: txactual.phone,
              group: invitedTx.group,
              role: invitedTx.role,
              approvee:txo.tx.approvee,
              publicKey: txo.tx.publicKey
            });
          }
        }
      });
    }
  });
  
  const objectPromise = ObjectTxPool.find({homeId: homeId}).then(objects => {
    
    if (objects.length > 0 ) {
      toSend.objects = objects.map(object => {
        let otx = JSON.parse(object.tx.tx);
        let encrypted = otx.encrypted;
    
        return {
          name: otx.objectInfo.name,
          description: otx.objectInfo.description,
          encrypted: encrypted,
          owner:object.user,
          ownerHome: object.tx.homeId,
          publicKey:object.tx.publicKey,
          status: "active"
        };
    
      });
      }});
  
  const PIPromise = PI.find({'tx.homeId': homeId}).then(PIs => {
    
    if (PIs.length > 0 ) {
      toSend.PIs = PIs.map(object => {
        let otx = JSON.parse(object.tx.tx);
        
        return {
          name: otx.name,
          account:object.tx.user,
          homeId: object.tx.homeId,
          publicKey:object.tx.publicKey,
          status: "active"
        };
        
      });
    }}
    
  );
  
  
  
  Promise.all([homePromise,objectPromise,PIPromise]).then(result => {
    
    if (found ) {
      res.send(toSend);
    } else {
      res.send({});
    }
  
  });
};


exports.getAccounts = function (req,res) {
  let homeId = req.query.home;
  
  
  const toSend = [];
  
  
  const homePromise = HomeCache.find({_id: homeId}).then(result => {
    if (result.length > 0) {
      
      const retired = [];
      const all = JSON.parse(JSON.stringify(result[0].txs));
      
      const reversed = all.reverse();
      
      
      reversed.forEach((tx, index) => {
        const txo = JSON.parse(tx.tx);
        
        if (txo.tx.action ==="newHome") {
          
          const txs = JSON.parse(txo.tx.txs);
          const adminAccount = txs.adminAccounts;
          
          toSend.push({
            name: adminAccount.name,
            
            email: adminAccount.email,
            phone: adminAccount.phone,
            group: "superAdmin",
            role: "admin",
            approvee:"genesis",
            publicKey: adminAccount.publicKey
          });
          return;
          
        }
  
        
        
        const insideTx = txo.tx.tx;
        
        if (!insideTx) return;
        
        const txactual = JSON.parse(insideTx);
  
        if (txo.tx.action === "retireAccount") {
          retired.push(txactual.account);
          return;
        }
        
        
        if (txo.tx.action === "newAccount") {
          
          if (retired.includes(txo.tx.owner)) {
            console.log("found retired " + txo.tx.owner);
          } else {
            
            const invitedTxs = txo.tx.invitedTxs;
            if (!invitedTxs) {
              return;
            }
            
            const invitedTx = JSON.parse(txo.tx.invitedTxs);
            
            
            toSend.push({
              name: txactual.name,
              
              email: txo.tx.owner,
              phone: txactual.phone,
              group: invitedTx.group,
              role: invitedTx.role,
              approvee:txo.tx.approvee,
              publicKey: txo.tx.publicKey
            });
          }
        }
        
      });
    }
  });
  
  
  Promise.all([homePromise]).then(result => {
    res.send(toSend);
  });
  
};

exports.getAccountInfo = function (req,res) {
  let homeId = req.query.home;
  let accountId = req.query.account;
  
  if (!homeId || homeId ==="" || !accountId || accountId ==="") {
    res.status(404).send("Not Found");
    return;
  }
  
  
  let toSend = {};
  let found = false;
  
  
  const homePromise = HomeCache.find({_id: homeId}).then(result => {
    if (result.length > 0) {
      
      const retired = [];
      const all = JSON.parse(JSON.stringify(result[0].txs));
      
      const reversed = all.reverse();
      
      
      reversed.forEach((tx, index) => {
        
        if (found) return;
        
        const txo = JSON.parse(tx.tx);
        
        if (txo.tx.action ==="newHome") {
          
          const txs = JSON.parse(txo.tx.txs);
          const adminAccount = txs.adminAccounts;
          
          if (accountId === adminAccount.email) {
            toSend.publicKey = adminAccount.publicKey;
            toSend.name = adminAccount.name;
            found = true;
            return;
          }
        }
        
        const insideTx = txo.tx.tx;
        
        if (!insideTx) return;
        
        const txactual = JSON.parse(insideTx);
        
        if (txo.tx.action === "retireAccount") {
          retired.push(txo.tx.account);
        }
        if (txo.tx.action === "newAccount") {
          
          if (retired.includes(txo.tx.owner)) {
            console.log("found retired " + txo.tx.owner);
          } else {
            
            const invitedTxs = txo.tx.invitedTxs;
            if (!invitedTxs) {
              return;
            }
            
            const invitedTx = JSON.parse(txo.tx.invitedTxs);
            const innerTx = JSON.parse(txo.tx.tx);
  
            if (accountId === txo.tx.owner) {
              toSend.publicKey = txo.tx.publicKey;
              toSend.name = innerTx.name;
              found = true;
            }
            
          }
        }
        
      });
    }
  });
  
  
  Promise.all([homePromise]).then(result => {
    res.send(toSend);
  });
};

exports.isExists = function (req,res) {
  
  let name = req.query.home;
  HomeCache.find({_id:name}).then(result => {
    if (result.length === 1) {
      res.send(true);
    } else {
      res.send(false);
    }
  })
  
};
