
var DocumentXchange = require('../models/DocumentXchange');

var CryptoHelper = require('../crypto/CryptoHelper');

var db = require('../db/DBConnections');



exports.index = function(req, res) {
  res.send('NOT IMPLEMENTED: Document storage... Index route for testing');
};





exports.get_sentbox = function (req,res) {
  let homeId = req.query.home;
  let accountId = req.query.account;
  
  
  DocumentXchange.find({'tx.senderHome':homeId,'tx.senderAccount':accountId}).then((result) => {
    res.send(result);
    
  }).catch((error) => {
    console.log("Error in getting objects for a home " + error);
  })
  
};




exports.get_inbox = function (req,res) {
  let homeId = req.query.home;
  let accountId = req.query.account;
  
  
  DocumentXchange.find({'tx.recipientHome':homeId,'tx.recipientAccount':accountId}).then((result) => {
    res.send(result);
    
  }).catch((error) => {
    console.log("Error in getting objects for a home " + error);
  })
  
};

exports.send_document = function(req, res) {
  
  const tx = req.body;
  
  // header field
  const senderHome = tx.senderHome;
  const senderAccount = tx.senderAccount;
  const recipientHome = tx.recipientHome;
  const recipientAccount = tx.recipientAccount;
  const senderPK = tx.senderPK;
  
  const hash = tx.hash;
  const signature = tx.signature;
  
  
  
  if (isEmpty(senderHome) || isEmpty(senderAccount) || isEmpty(recipientHome)
    || isEmpty(recipientAccount) || isEmpty(hash) || isEmpty(signature) || isEmpty(senderPK)
  
  ) {
    return res.send("Invalid transaction..");
    
  } else {
  
    const result = CryptoHelper.validateSignature(senderPK, hash, signature);
  
    if (!result) {
      res.send({success: false, error: "Signature validation failed..."})
    
    } else {
      const documentX = new DocumentXchange({_id:tx.hash,tx:tx});
  
      documentX.save().then((result, error) => {
        
        if (!error) {
          res.status(200).send({success: true});
        } else {
          res.status(403).send({success: false});
        }
        
      });
    }
  }
  
};


exports.get_history = function (req,res) {

  let fileId = req.query.fileId;
  
  let toSend = [];
  
  DocumentXchange.find({'tx.fileId':fileId}).then((result) => {
    
    result.map((shared => {
      toSend.push({fileId:fileId,recipientHomeId:shared.tx.recipientHome, recipientAccount:shared.tx.recipientAccount,date:shared.tx.timeStamp });
      
    }));
    
    res.send(toSend);
    
    
    
  }).catch((error) => {
    console.log("Error in getting objects for a home " + error);
  })
  
};



function isEmpty(str) {
  return (!str || 0 === str.length);
}