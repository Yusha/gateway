
let CryptoHelper = require('../crypto/CryptoHelper');
let mongoose = require('mongoose');
let HomeCache = require('../models/HomeCache');
let HomeTx = require('../models/HomeTxPool');
let Invitation = require ('../models/Invitation');



let mongoDB = 'mongodb://127.0.0.1/home';

mongoose.connect(mongoDB, { useNewUrlParser: true });
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));




exports.index = function(req, res) {
  res.send('NOT IMPLEMENTED: Home Ledger. Index route for testing');
};


// Create a new home request
exports.create_home = function(req, res) {
  
  handleTransaction(req,res);
};


exports.create_adminAccount = function(req, res) {
  handleTransaction(req,res);
  
};


exports.retire_adminAccount = function(req, res) {
};

exports.create_home_phone = function(req, res) {
  handleTransaction(req,res);
};


exports.retire_home_phone = function(req, res) {
  handleTransaction(req,res);
};


exports.create_home_email = function(req, res) {
  handleTransaction(req,res);
};


exports.retire_home_email = function(req, res) {
  handleTransaction(req,res);
};


exports.create_account = function(req, res) {
  handleTransaction(req,res);
};


exports.retire_account = function(req, res) {
  handleTransaction(req,res);
};

exports.create_address = function(req, res) {
  handleTransaction(req,res);
};


exports.retire_address = function(req, res) {
  handleTransaction(req,res);
};

exports.create_group = function(req, res) {
  handleTransaction(req,res);
};


exports.retire_group = function(req, res) {
  handleTransaction(req,res);
};


exports.create_role = function(req, res) {
  handleTransaction(req,res);
};


exports.retire_role = function(req, res) {
  handleTransaction(req,res);
};

exports.invite = function(req, res) {
  
  const tx = req.body.tx;
  
  const hash = tx.hash;
  const signature = tx.signature;
  const homeId = tx.homeId;
  const publicKey = tx.publicKey;
  const action = tx.action;
  const invitee = tx.invitee;
  const user = tx.user;
  
  
  if ("invite" !== action) {
    res.send("Invalid action type..");
    return;
  }
  
  
  if (isEmpty(hash) ||
    isEmpty(signature) ||
    isEmpty(homeId) ||
    isEmpty(publicKey) ||
    isEmpty(invitee) ||
    isEmpty(user)
  ) {
    res.send("Empty parameters found in the transaction..");
    return;
  }
  const result = CryptoHelper.validateSignature(publicKey,hash,signature);
  
  if (!result) {
    res.send("Invalid Signature in the transaction..");
    return;
  }
  
  // validate homeId
  HomeCache.find({_id:homeId}).then(result => {
    if (result.length === 0) {
      res.send("Invalid HomeId");
      return;
    }
  
    const invitationTx = new Invitation({_id:hash,timeStamp:new Date(),action:action,homeId:homeId,invitee:invitee,user:user,tx:tx,approved:false,claimed:false,validated:false});
    invitationTx.save();
    
  }).then((result) => {
  
    console.log("Invitation saved in the code...");
    res.send("Invitation processed...");
    return;
    
  }).catch((error) => {
  
    console.log("error in saving the invitation " + error);
    res.send("Error in saving transaction......");
    return;
    
  });
    
    // TBD: Also check if the invitation already exists
  
};


exports.claim_invite = function (req, res) {
  
  
  const tx = req.body.tx;
  const hash = tx.hash;
  const signature = tx.signature;
  const homeId = tx.homeId;
  const publicKey = tx.publicKey;
  const action = tx.action;
  const invitee = tx.invitee;
  const user = tx.user;
  
  
  if (isEmpty(hash) ||
    isEmpty(signature) ||
    isEmpty(homeId) ||
    isEmpty(publicKey) ||
    isEmpty(invitee) ||
    isEmpty(user)
  ) {
    res.send("Empty paramters found in the transaction..");
    return;
  }
  const result = CryptoHelper.validateSignature(publicKey,hash,signature);
  
  if (!result) {
    res.send("Invalid Signature in the transaction..");
    return;
  }
  
  // validate homeId
  HomeCache.find({_id:homeId}).then((result) => {
    if (result.length === 0) {
      res.send("Invalid HomeId");
      return;
    }
    // TBD: Also check if the approver (user) is same who sent the invitation.
    return Invitation.find({homeId:homeId,invitee:invitee});
  }).then ((result) => {
    
    const toUpdate = result[0];
    
    toUpdate.claimedTx = tx;
    toUpdate.claimed = true;
    return toUpdate.save();
    
  }).then((result)=> {
    
    console.log("Tx was updated  " );
    res.send({processed:true});
    return;
    
  }).catch( (error) => {
      console.log("Error in Approval Tx saving..." + error);
      return res.send({processed:false,error:"Error in procesing TX"});
    
  })
};


exports.validate_invite = function(req, res) {

  res.send("Not implemented yet");
  
};

exports.approve_invite = function(req, res) {
  
  const tx = req.body.tx;
  const hash = tx.hash;
  const signature = tx.signature;
  const homeId = tx.homeId;
  const publicKey = tx.publicKey;
  const action = tx.action;
  const invitee = tx.invitee;
  const user = tx.user;
  
  const approveePK = tx.approveePK;
  
  if (isEmpty(hash) ||
    isEmpty(signature) ||
    isEmpty(homeId) ||
    isEmpty(publicKey) ||
    isEmpty(invitee) ||
    isEmpty(user)
  ) {
    res.send("Empty parameters found in the transaction..");
    return;
  }
  const result = CryptoHelper.validateSignature(approveePK,hash,signature);
  
  if (!result) {
    res.send("Invalid Signature in the transaction..");
    return;
  }
  
  // validate homeId
  HomeCache.find({_id:homeId}).then((result) => {
    if (result.length === 0) {
      res.send("Invalid HomeId");
      return;
    }
      // TBD: Also check if the approver (user) is same who sent the invitation.
      return Invitation.findOne({_id: hash});
    }).then ((result) => {
    
      // found the invitee. Now update it with the approval tx.
      result.approvalTx = tx;
      result.approved = true;
      return result.save();
    
    
    }).then((result)=> {
  
    const claimedTx = result.claimedTx;
    const accountTx = {
        hash:tx.hash,
        signature: claimedTx.hash,
        homeId:    claimedTx.homeId,
        publicKey: claimedTx.publicKey,
        action:"newAccount",
        owner: claimedTx.user,
        approvee: result.approvalTx.user,
        tx:claimedTx.tx,
        invitedTxs:result.tx.tx
    };
      
      // write a transaction to the Tx pool.
    const newTx = new HomeTx({_id:accountTx.hash,txHash:accountTx.hash,homeId:accountTx.homeId,ledgerId:0,tx:accountTx});
    return newTx.save();
  }).then((result)=> {
    
    console.log("Tx was updated  ");
    return res.send({processed:true});
    
  }).catch( (error) => {
    console.log("Error in Approval Tx saving..." + error);
    return res.send({processed:false,error:"Error in procesing TX"});
    
  })
};

exports.get_home_details = function(req, res) {
  
  let name = req.query.home;
  HomeCache.find({_id:name}).then(result => {
    res.send(JSON.stringify(result));
  })
  
};


// http://localhost:4000/home/pendingInvitations?home=Microsoft
exports.get_pendingInvitations = function(req, res) {
  
  let name = req.query.home;
  
  Invitation.find({homeId:name,claimed:true}).then(result => {
    res.send(JSON.stringify(result));
  })
};

exports.get_approvedInvitations = function(req, res) {
  
  let name = req.query.home;
  
  Invitation.find({homeId:name,approved:true}).then(result => {
    res.send(JSON.stringify(result));
  })
};


exports.invitationExists = function(req, res) {
  const tx = req.body.tx;
  const homeId = tx.homeId;
  const invitee = tx.invitee;
  
  Invitation.find({homeId:homeId,invitee:invitee}).then(result => {
    if (result.length > 0) {
      return res.send({exists:true});
    } else {
      return res.send({exists:false});
    }
  })
};


// http://localhost:4000/home/groups?home=Microsoft
exports.get_groups = function(req, res) {
  
  let name = req.query.home;
  
  HomeTx.find({homeId:name,'tx.action':"createGroup"}).then(result => {
    res.send(JSON.stringify(result));
  })
};


exports.get_roles = function(req, res) {
  
  let name = req.query.home;
  
  HomeTx.find({homeId:name,'tx.action':"createRole"}).then(result => {
    res.send(JSON.stringify(result));
  })
};

// http://localhost:4000/home/account?home=Microsoft&account=akbar3@microsoft.com
// Returns account public Key


exports.get_account = function(req, res) {
  
  let homeId = req.query.home;
  let account = req.query.account;
  let query = {
    homeId:homeId,
    'tx.action':"newAccount",
    'tx.owner':account
  };
  
  HomeTx.find(query).then(result => {
    if (result.length === 1) {
      let found = result[0];
      let name = JSON.parse(found.tx.tx).name;
      
      res.send( {publicKey:found.tx.publicKey,name:name});
    } else {
      console.log('Account not found');
      res.send(404);
    }
    
  })
};

exports.get_accounts = function(req, res) {
  
  let name = req.query.home;
  
  HomeTx.find({homeId:name,'tx.action':"newAccount"}).then(result => {
    res.send(JSON.stringify(result));
  })
};


exports.get_status= function(req, res) {
  
  let txId = req.query.txId;
  
  HomeTx.find({_id:txId}).then(result => {
    if (result.length === 0) {
      res.send('Tx not found');
    } else {
  
      let ledgerId = result[0].ledgerId;
  
      if (ledgerId === 0) {
        res.send("Status: Pending");
      } else {
        res.send("Tx Ledger Id " + ledgerId);
      }
    }
  })
  
};

function handleTransaction(req, res) {
  
  const tx = req.body.tx;
  
  // header field
  const hash = tx.hash;
  const signature = tx.signature;
  const homeId = tx.homeId;
  const publicKey = tx.publicKey;
  const action = tx.action;
  
  if (isEmpty(hash) ||
      isEmpty(signature) ||
      isEmpty(homeId) ||
      isEmpty(publicKey) ||
      isEmpty(action)
      ) {
    res.send("Invalid transaction..");
    return;
  }
  
  // Validate signature
  const result = CryptoHelper.validateSignature(publicKey,hash,signature);
  // TODO: Validate other home related logic...
  
  if (result) {
    
    const newTx = new HomeTx({_id:hash,txHash:hash,homeId:homeId,ledgerId:0,tx:tx});
    // const result = await newTx.save();
    newTx.save((result) => {
      console.log("Success saving..Result is  " + JSON.stringify(result));
      res.send("The Request is in process " + result);
    });
    
  } else {
    res.send('Invalid request signature not valid');
  }
}

function isEmpty(str) {
  return (!str || 0 === str.length);
}
