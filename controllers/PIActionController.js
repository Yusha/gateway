
let Task = require('../models/Task');
let PI = require('../models/PI');

let CryptoHelper = require('../crypto/CryptoHelper');

let db = require('../db/DBConnections');



exports.index = function(req, res) {
  res.send('NOT IMPLEMENTED: Document storage... Index route for testing');
};




exports.get_PI = function (req,res) {
  let homeId = req.query.homeId;
  let approver = req.query.approver;
  
  
  Task.find({homeId:homeId,approver:approver}).then((result) => {
    res.send(result);
    
  }).catch((error) => {
    console.log("Error in getting objects for a home " + error);
  })
  
};

exports.approve_PI = function(req, res) {
  
  const tx = req.body.tx;
  
  // header field
  const homeId = tx.homeId;
  const accountId = tx.approver;
  const hash = tx.hash;
  const signature = tx.signature;
  const publicKey = tx.publicKey;
  const taskId = tx.taskId;
  
  
  if (isEmpty(hash) || isEmpty(signature) || isEmpty(homeId)
    || isEmpty(accountId || isEmpty(publicKey))
  
  ) {
    return res.send("Invalid transaction..");
  } else {
    
    const result = CryptoHelper.validateSignature(publicKey, hash, signature);
    
    if (!result) {
      res.send({success: false, error: "Signature validation failed..."})
      
    } else {
      
      // update the PI with the approver Task
      return PI.find({_id:hash}).then((found) => {
        const fPI = found[0];
        fPI.approvingTx = tx;
        return fPI.save();
        
      }).then((saved) => {
        // update the task status
        console.log("Saved approvingTx");
        return Task.find({_id:taskId});
      }). then((foundTask) => {
        const fTask = foundTask[0];
        fTask.approved = true;
        return fTask.save();
        
      }).then((taskSaved) => {
        console.log("Task status saved to true");
        res.status(200).send({success:true});
      }).
      catch ((error) => {
        res.status(403).send({success:false, error:error});
      })
      
    }
  }
};


function isEmpty(str) {
  return (!str || 0 === str.length);
}