let PI = require('../models/PI');
let Task = require('../models/Task');

let CryptoHelper = require('../crypto/CryptoHelper');

let db = require('../db/DBConnections');



exports.index = function(req, res) {
  res.send('NOT IMPLEMENTED: Document storage... Index route for testing');
};




exports.get_PIO = function (req,res) {
  let id = req.query.id;
  
  
  PI.find({_id:id}).then((result) => {
    if (result.length === 1) {
      res.send(result[0]);
    }
    
  }).catch((error) => {
    console.log("Error in getting objects for a home " + error);
  })
  
};


exports.get_PI = function (req,res) {
  let homeId = req.query.homeId;
  
  
  PI.find({'tx.homeId':homeId}).then((result) => {
    res.send(result);
    
  }).catch((error) => {
    console.log("Error in getting objects for a home " + error);
  })
  
};
exports.add_PI = function(req, res) {
  
  const tx = req.body;
  
  // header field
  const homeId = tx.homeId;
  const accountId = tx.user;
  const hash = tx.hash;
  const signature = tx.signature;
  const publicKey = tx.publicKey;
  const approver = tx.approver;
  
  
  
  if (isEmpty(hash) || isEmpty(signature) || isEmpty(homeId)
    || isEmpty(accountId || isEmpty(publicKey))
  
  ) {
    return res.send("Invalid transaction..");
  } else {
    
    const result = CryptoHelper.validateSignature(publicKey, hash, signature);
    
    if (!result) {
      res.send({success: false, error: "Signature validation failed..."})
      
    } else {
      
      const newPI = new PI({_id:hash,blockId:0,tx:tx});
      
      
      return newPI.save().then((saved)=> {
        
        
        console.log("PI is saved");
  
        if (approver) {
          const taskId = hash + "/" +  accountId;
          const task = new Task({_id:taskId,action:"New PI",homeId:homeId,accountId:accountId,approver:approver, approverHome:homeId,
            appId:"PI",appReference:hash,timeStamp:new Date(),approved:false});
            
          return task.save();
          
        } else {
          console.log("Task will not be created as there is no approver set...")
          res.status(200).send({success: true});
        }
        
        }).then((tasked) => {
          console.log("Task created and success sent");
          res.status(200).send({success: true});
        
      }).
        catch ((err) => {
        console.log("Error in saving something");
          res.status(403).send({success: false});
        
      })
      
    }
  }
};

exports.deactivate_PIO = function(req, res) {
  
  const tx = req.body;
  
  // header field
  const PIO = tx.PIO;
  const hash = tx.hash;
  const signature = tx.signature;
  const publicKey = tx.publicKey;
  
  
  
  if (isEmpty(PIO) || isEmpty(signature) || isEmpty(hash)
    || isEmpty(publicKey)) {
    return res.send("Invalid transaction..");
  } else {
    
    const result = CryptoHelper.validateSignature(publicKey, hash, signature);
    
    if (!result) {
      res.send({success: false, error: "Signature validation failed..."})
      
    } else {
      
      // find the PIO and update the task..
      PI.find({_id:PIO}).then((result) => {
        if (result && result.length === 1) {
          
          const thisPIO = result[0];
          thisPIO.retireTx = tx;
          thisPIO.retireBlockId = 0;
          return thisPIO.save();
        } else {
          res.status(404).send({success:false});
        }
    
      }).then((saved) => {
        if (saved)
          res.status(200).send({success:true})
        
      }).catch((error) => {
        res.status(403).send({success:false, error:error})
        
      })
    }
  }
};




function isEmpty(str) {
  return (!str || 0 === str.length);
}