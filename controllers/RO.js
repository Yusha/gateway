var CryptoHelper = require('../crypto/CryptoHelper');
var ObjectPoolTx = require('../models/ObjectTxPool');
let SharedObject = require('../models/SharedObject');

var db = require('../db/DBConnections');



exports.index = function(req, res) {
  res.send('NOT IMPLEMENTED: Home Ledger. Index route for testing');
};


// Create a new home request
exports.create_object = function(req, res) {
  
  if (! ("newObject" === req.body.tx.action)) {
    res.send("Bad transaction... invalid request type");
  } else {
    handleTransaction(req,res);
  }
  
};

exports.updateObject = function(req, res) {
  if (! ("updateObject" === req.body.tx.action)) {
    res.send("Bad transaction... invalid request type");
  } else {
    handleTransaction(req,res);
  }
};

exports.retireObject = function(req, res) {
  
  if (! ("retireObject" === req.body.tx.action)) {
    res.send("Bad transaction... invalid request type");
  } else {
    handleTransaction(req,res);
  }
  
};


exports.get_objects = function (req,res) {
  let name = req.query.home;
  
  
  ObjectPoolTx.find({homeId:name}).then((result) => {
    res.send(result);
    
  }).catch((error) => {
    console.log("Error in getting objects for a home " + error);
  })
  
};

exports.get_shared = function (req,res) {
  let home = req.query.home;
  let account = req.query.account;
  
  
  SharedObject.find({'tx.recipientHome':home,'tx.recipientAccount':account}).then((result) => {
    res.send(result);
    
  }).catch((error) => {
    console.log("Error in getting objects for a home " + error);
  })
  
};


exports.shareObject = function(req, res) {
  
  const tx = req.body;
  
  // header field
  const senderHome = tx.senderHome;
  const senderAccount = tx.senderAccount;
  const recipientHome = tx.recipientHome;
  const recipientAccount = tx.recipientAccount;
  const senderPK = tx.senderPK;
  const recipientPK = tx.recipientPK;
  
  const hash = tx.hash;
  const signature = tx.signature;
  
  
  
  if (isEmpty(hash) || isEmpty(signature)
    || isEmpty(senderAccount) || isEmpty(senderHome) || isEmpty(senderPK)
    || isEmpty(recipientAccount) || isEmpty(recipientHome) || isEmpty(recipientPK)
  
  ) {
    res.send("Invalid transaction.. Parameters are missing");
    return;
  }
  
  
  const result = CryptoHelper.validateSignature(senderPK,hash,signature);
  
  // TODO: Validate other things...
  
  if (!result) {
    res.send({success:false,error:"Signature validation failed..."})
    
  } else {
    
    const sharedObject = new SharedObject({_id:hash,timeStamp:new Date(),tx:tx});
  
    sharedObject.save().then((result) =>  {
      
      console.log("Saved..Share..");
      
      return res.send({success:true});
      
    }).catch((error => {
      return res.send({success:false,error:error});
    }))
  }
  
};

function handleTransaction(req, res) {
  
  const tx = req.body.tx;
  
  // header field
  const homeId = tx.homeId;
  const objectId = tx.objectId;
  const nonce = tx.nonce;
  const user = tx.user;
  
  const hash = tx.hash;
  const signature = tx.signature;
  const publicKey = tx.publicKey;
  const action = tx.action;
  
  
  if (isEmpty(hash) || isEmpty(signature) || isEmpty(homeId) || isEmpty(publicKey) || isEmpty(action)
  ) {
    console.log("Invalid Transaction Received for newObject")
    res.send("Invalid transaction..");
    return;
  }
  
  // Validate signature
  const result = CryptoHelper.validateSignature(publicKey,hash,signature);
  // TODO: Validate other things...
  
  if (result) {
  
    const objectTx = new ObjectPoolTx({_id:hash,homeId:homeId,objectId:objectId,user:user,ledgerId:0,nonce:nonce,tx:tx});
    objectTx.save(function (error, result) {
      if (result) {
        console.log("Object saved successfully... "  + JSON.stringify(result));
        res.send("The transaction is processed. The track Id is "  + hash);
      } else {
        console.log("Error  in saving Object... " + JSON.stringify(error));
        res.send("Error in processing the object transaction.. try again ");
      }
    })
    
  }
  
  else {
    console.log("Cryptographic validation of the New object failed...");
    res.send('Invalid Object Request');
  }
  
}




function isEmpty(str) {
  return (!str || 0 === str.length);
}