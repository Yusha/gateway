// Every home transactions are driven by a policy
// Home will have full control on its policy
// The ledger operators will ensure that any home transaction conforms to the policy guidelines
// The policies can be as simple as key value pair or as complex as a code function.
// The policy functions are smart contract code which runs on the operator side.
