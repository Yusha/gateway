
let Contract = require('../models/Contract');
let CTemplate = require('../models/CTemplate');
let CCode = require('../models/ContractCode');
let SignTask = require('../models/SignTask');
let Signature = require('../models/Signature');

let CryptoHelper = require('../crypto/CryptoHelper');

let db = require('../db/DBConnections');



exports.index = function(req, res) {
  res.send('NOT IMPLEMENTED: Document storage... Index route for testing');
};



exports.add = function(req, res) {
  
  const tx = req.body;
  
  // header field
  const senderHome = tx.originator;
  const senderAccount = tx.senderAccount;
  const hash = tx.hash;
  const signature = tx.signature;
  const publicKey = tx.senderPublicKey;
  const originator = tx.originator;
  const header = tx.headerData;
  
  
  
  
  if (isEmpty(hash) || isEmpty(signature) || isEmpty(senderHome)
    || isEmpty(senderAccount || isEmpty(publicKey))
  
  ) {
    return res.send("Invalid transaction..");
  } else {
  
    const result = CryptoHelper.validateSignature(publicKey, hash, signature);
  
    if (!result) {
      res.send({success: false, error: "Signature validation failed..."});
    
    } else {
      
      // keep the tasks ready and save them immediately after contract is saved
      
      const signTasks = header.partiesData.map((party,index) => {
        const id = hash + "/" +party.homeId + "/" + party.accountId;
        
        return { insertOne :
          {
            document: {_id:id,homeId:party.homeId, accountId:party.accountId,contractId:hash,name:header.name,timeStamp:new Date(),completed:false}
          }
        }
      });
      
      // now save the contract
      const contract = new Contract({_id:hash,originator:originator,header:header,tx:tx});
      
      contract.save((error, result) => {
        if (error) {
          console.log("error saving Contract  " + error);
          res.status(402).send({success:false,error:error});
        } else {
          
          console.log("Success saving Contract..");
          SignTask.collection.bulkWrite(signTasks).then(result => {
            console.log("Bulk Tasks saved");
            res.status(200).send({success: true});

          }).catch( error => {
            console.log("Error in saving sign task");
            res.status(402).send({success: false});
  
          });
        }
      });
    }
  }
  
};

exports.sign = function(req, res) {
  
  const tx = req.body;
  
  // header field
  const partyHome = tx.partyHome;
  const partyAccount = tx.partyAccount;
  const hash = tx.contractId;
  const signature = tx.signature;
  const publicKey = tx.publicKey;
  const contractId = tx.contractId;
  
  
  
  if (isEmpty(hash) || isEmpty(signature) || isEmpty(publicKey)
    || isEmpty(partyHome || isEmpty(partyAccount) || isEmpty(publicKey))
  
  ) {
    return res.send("Invalid transaction..");
  } else {
    
    const result = CryptoHelper.validateSignature(publicKey, hash, signature);
    
    if (!result) {
      res.send({success: false, error: "Signature validation failed..."});
      
    } else {
      // Add the Signature
      let id = hash + "/" + partyHome + "/" + partyAccount;
      let timeStamp = new Date();
      
      try {
        let signature = new Signature({_id: id, timeStamp: timeStamp, ledgerId: 0, tx: tx});
        
        signature.save().then(saved => {
          
          // signature is saved. now get updated the SignTask
          
          SignTask.findOne({_id:id}).then(found => {
            found.completed = true;
            return found.save();
            
          })
        }).then(updated => {
          console.log("Found SignTak and updated");
          // now update Contracts collection
          
          return Contract.findOne({_id:hash});
        
        }).then ((contract) => {
          console.log("Found corresponding contract for the signature task");
          
          contract.signatures.push({tx:tx,timeStamp:timeStamp});
          return contract.save();
          
        }).then(savedContract => {
          console.log("Signature tx saved in Contract cache");
           res.status(200).send({success: true});
        
        }).catch(error => {
          console.log("Error in Saving Signature and updating SignatureTask" + error);
          res.status(403).send({success: false,error:error});
        });
        
      } catch (exception) {
        console.log("Exception in Saving Signature and updating SignatureTask" + exception);
        res.status(403).send({success: false,error:error});
      }
      
    }
  }
  
};



exports.get = function (req,res) {
  
  let originator = req.query.originator;
  
  Contract.find({originator:originator}).then((result) => {
    res.send(result);
    
  }).catch((error) => {
    console.log("Error in getting objects for a home " + error);
  })
};

exports.getContract = function (req,res) {
  
  let contractId = req.query.contractId;
  
  Contract.find({_id:contractId}).then((result) => {
    if (result.length === 1) {
      res.send(result[0]);
    } else {
      res.send([]);
    }
  }).catch((error) => {
    console.log("Error in getting objects for a home " + error);
    res.send([]);
  })
};


// Get Contract Template and code for a given contract
exports.getContractData = function (req,res) {
  
  const templateId = req.query.templateId;
  const codeId = req.query.codeId;
  const contractId = req.query.contractId;
  
  const toSend = {
    templateData: {},
    codData: {},
    signatures:[]
  };
  
  const templatePromise = CTemplate.find({_id: templateId}).then(result => {
    if (result.length === 1) {
      toSend.templateData = JSON.parse(result[0].tx.body);
    }
  });
  
  const codePromise = CCode.find({_id: codeId}).then(result => {
    if (result.length === 1) {
      toSend.codeData = JSON.parse(result[0].tx.body);
    }
  });
  
  const signaturePromise = Signature.find({'tx.contractId':contractId}).then(result => {
    toSend.signatures = result;
  });
  
  
  Promise.all([templatePromise,codePromise,signaturePromise]).then(result => {
    res.send(toSend);
  });
  
};

exports.getSignatures = function (req,res) {
  
  let contractId = req.query.contractId;
  
  Signature.find({'tx.contractId':contractId}).then((result) => {
    res.send(result);
    
  }).catch((error) => {
    console.log("Error in getting contract Signatures " + error);
  })
};

exports.getSignature = function (req,res) {
  
  let partyHome = req.query.homeId;
  let partyAccount = req.query.account;
  let contractId = req.query.contractId;
  let id = contractId + "/" + partyHome + "/" + partyAccount;
  
  
  Signature.findOne({_id:id}).then((result) => {
      res.send(result);
      
  }).catch((error) => {
    console.log("Error in getting Contract Signature " + error);
  })
};

exports.getContractForType = function (req,res) {
  
  let templateId = req.query.templateId;
  
  Contract.find({'tx.headerData.templateId':templateId}).then((result) => {
    res.send(result);
  }).catch((error) => {
    console.log("Error in getting contracts for the template Id " + error);
    res.send([]);
  })
};



function isEmpty(str) {
  return (!str || 0 === str.length);
}