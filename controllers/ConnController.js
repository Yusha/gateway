
let Connection = require('../models/Connection');

let CryptoHelper = require('../crypto/CryptoHelper');

let db = require('../db/DBConnections');



exports.index = function(req, res) {
  res.send('NOT IMPLEMENTED: Network Connections... Index route for testing');
};




exports.get_connections = function (req,res) {
  let home = req.query.home;
  
  Connection.find({'tx.homeId':home}).then((result) => {
    res.send(result);
  }).catch((error) => {
    console.log("Error in getting objects for a home " + error);
  })
  
};


exports.add_connection = function(req, res) {
  
  const tx = req.body;
  
  // header field
  const homeId = tx.homeId;
  const hash = tx.hash;
  const signature = tx.signature;
  const publicKey = tx.publicKey;
  
  
  
  if (isEmpty(hash) || isEmpty(signature) || isEmpty(homeId) || isEmpty(publicKey))
  
  {
    return res.send("Invalid transaction..");
  } else {
    
    const result = CryptoHelper.validateSignature(publicKey, hash, signature);
    
    if (!result) {
      res.status(403).send({success: false, error: "Signature validation failed..."})
      
    } else {
      const newConn = new Connection({_id:hash,blockId:0,timeStamp:new Date(),tx:tx});
      newConn.save().then((saved)=> {
        console.log("Network Connection got saved");
        res.status(200).send({success:true});
      });
    }
  }
};


function isEmpty(str) {
  return (!str || 0 === str.length);
}