
let Document = require('../models/Document');

let CryptoHelper = require('../crypto/CryptoHelper');

let db = require('../db/DBConnections');



exports.index = function(req, res) {
  res.send('NOT IMPLEMENTED: Document storage... Index route for testing');
};




exports.get_documents = function (req,res) {
  let homeId = req.query.home;
  let accountId = req.query.account;
  
  
  Document.find({'tx.ownerHome':homeId,'tx.ownerAccount':accountId}).then((result) => {
    res.send(result);
    
  }).catch((error) => {
    console.log("Error in getting objects for a home " + error);
  })
  
};

exports.add_document = function(req, res) {
  
  const tx = req.body;
  
  // header field
  const ownerHomeId = tx.ownerHome;
  const ownerAccount = tx.ownerAccount;
  const hash = tx.hash;
  const signature = tx.signature;
  const publicKey = tx.publicKey;
  
  
  
  if (isEmpty(hash) || isEmpty(signature) || isEmpty(ownerHomeId)
    || isEmpty(ownerAccount || isEmpty(publicKey))
  
  ) {
    return res.send("Invalid transaction..");
  } else {
  
    const result = CryptoHelper.validateSignature(publicKey, hash, signature);
  
    if (!result) {
      res.send({success: false, error: "Signature validation failed..."})
    
    } else {
      const newDocument = new Document({_id:hash,tx:tx});
      return newDocument.save().then((result, error) => {
        console.log("Error is  " + error);
        res.status(200).send({success:true});
      });
    }
  }
  
};


function isEmpty(str) {
  return (!str || 0 === str.length);
}