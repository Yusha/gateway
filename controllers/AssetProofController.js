let AssetProof = require('../models/AssetProof');

let CryptoHelper = require('../crypto/CryptoHelper');

let db = require('../db/DBConnections');



exports.index = function(req, res) {
  res.send('NOT IMPLEMENTED: Asset Proofs... Index route for testing');
};



exports.get = function (req,res) {
  let homeId = req.query.homeId;
  
  
  AssetProof.find({homeId:homeId}).then((result) => {
    res.send(result);
    
  }).catch((error) => {
    console.log("Error in getting objects for a home " + error);
  })
  
};

exports.getAsset = function (req,res) {
  let assetId = req.query.assetId;
  
  
  AssetProof.find({_id:assetId}).then((result) => {
    if (result) {
      if (result.length === 1) {
        res.status(200).send(result[0]);
      } else {
        res.status(200).send("");
      }
    }
  }).catch((error) => {
    console.log("Error in getting objects for a home " + error);
  })
  
};


exports.add = function(req, res) {
  
  const tx = req.body;
  
  // header field
  const homeId = tx.homeId;
  const homeAccount = tx.homeAccount;
  const hash = tx.hash;
  const signature = tx.signature;
  const publicKey = tx.publicKey;
  
  
  
  
  if (isEmpty(hash) || isEmpty(signature) || isEmpty(homeId)
    || isEmpty(homeAccount || isEmpty(publicKey))
  
  ) {
    return res.send("Invalid transaction..");
  } else {
  
    const result = CryptoHelper.validateSignature(publicKey, hash, signature);
  
    if (!result) {
      res.send({success: false, error: "Signature validation failed..."});
    
    } else {
      const assetProof = new AssetProof({_id:tx.body.assetHash,blockId:0,homeId:homeId,tx:tx});
      assetProof.save().then((result, error) => {
        if (error) {
          console.log("error saving ctemplate  " + error);
          res.status(402).send({success:false,error:error});
        } else {
          console.log("Success Asset Proof");
          res.status(200).send({success: true});
        }
      });
    }
  }
  
};



function isEmpty(str) {
  return (!str || 0 === str.length);
}