

let SignTask = require('../models/SignTask');



exports.index = function(req, res) {
  res.send('NOT IMPLEMENTED: Document storage... Index route for testing');
};


exports.get_SignTasks = function (req,res) {
  let homeId = req.query.homeId;
  let accountId = req.query.accountId;
  
  
  SignTask.find({homeId:homeId,accountId:accountId}).then((result) => {
    if (result.length >0 ) {
      res.send(result);
    } else {
      res.send([]);
    }
    
  }).catch((error) => {
    console.log("Error in getting Signature Tasks " + error);
    res.send([]);
  })
};
