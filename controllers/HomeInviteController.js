
let HomeInvite = require('../models/HomeInvite');

let CryptoHelper = require('../crypto/CryptoHelper');

let db = require('../db/DBConnections');



exports.index = function(req, res) {
  res.send('NOT IMPLEMENTED: Network Connections... Index route for testing');
};





exports.get_invite = function (req,res) {
  let invitee = req.query.invite;
  
  HomeInvite.find({'tx.invitee':invitee}).then((result) => {
    res.send(result);
  }).catch((error) => {
    console.log("Error in getting objects for a home " + error);
  })
  
};

// http://localhost:4000/homeInvite/get?invite=akbar@ansari.com

exports.add_invite = function(req, res) {
  
  const tx = req.body;
  
  // header field
  const homeId = tx.homeId;
  const hash = tx.hash;
  const signature = tx.signature;
  const publicKey = tx.publicKey;
  
  
  
  if (isEmpty(hash) || isEmpty(signature) || isEmpty(homeId) || isEmpty(publicKey))
  
  {
    return res.send("Invalid transaction..");
  } else {
    
    const result = CryptoHelper.validateSignature(publicKey, hash, signature);
    
    if (!result) {
      res.status(403).send({success: false, error: "Signature validation failed..."})
      
    } else {
      const homeInvite = new HomeInvite({_id:hash,blockId:0,timeStamp:new Date(),tx:tx});
      homeInvite.save().then((saved)=> {
        console.log("Home Invite got saved");
        res.status(200).send({success:true});
      });
    }
  }
};


exports.exists = function(req, res) {
  
  const tx = req.body;
  
  // header field
  const inviterHome = tx.inviterHome;
  const inviterAccount = tx.inviterAccount;
  const invitee = tx.invitee;
  const inviteSecret = tx.inviteSecret;
  
  
  
  if (isEmpty(inviterHome) || isEmpty(inviterAccount) || isEmpty(invitee) || isEmpty(inviteSecret))
  
  {
    return res.send({exists:false});
  } else {
  
    HomeInvite.find({'tx.homeId':inviterHome,'tx.user':inviterAccount,'tx.invitee':invitee,'tx.secret':inviteSecret}).then((result) => {
      if (result.length >0) {
        res.send({exists:true});
      } else {
        res.send({exists:false});
      }
    }).catch((error) => {
      console.log("Error in getting objects for a home " + error);
      res.send({exists:false});
    })
  }
};






function isEmpty(str) {
  return (!str || 0 === str.length);
}