
let DocumentContract = require('../models/DocumentContract');
let CCode = require('../models/ContractCode');

let CryptoHelper = require('../crypto/CryptoHelper');

let db = require('../db/DBConnections');



exports.index = function(req, res) {
  res.send('NOT IMPLEMENTED: Document Contract... Index route for testing');
};



exports.get = function (req,res) {
  let originator = req.query.originator;
  
  
  DocumentContract.find({originator:originator}).then((result) => {
    res.send(result);
    
  }).catch((error) => {
    console.log("Error in getting objects for a home " + error);
  })
  
};


exports.add = function(req, res) {
  
  const tx = req.body;
  
  // header field
  const senderHome = tx.originator;
  const senderAccount = tx.senderAccount;
  const hash = tx.hash;
  const signature = tx.signature;
  const publicKey = tx.senderPublicKey;
  const originator = tx.originator;
  const header = tx.headerData;
  
  
  
  
  if (isEmpty(hash) || isEmpty(signature) || isEmpty(senderHome)
    || isEmpty(senderAccount || isEmpty(publicKey))
  
  ) {
    return res.send("Invalid transaction..");
  } else {
  
    const result = CryptoHelper.validateSignature(publicKey, hash, signature);
  
    if (!result) {
      res.send({success: false, error: "Signature validation failed..."});
    
    } else {
      const contract = new DocumentContract({_id:hash,originator:originator,header:header,tx:tx});
      contract.save().then((result, error) => {
        if (error) {
          console.log("error saving ctemplate  " + error);
          res.status(402).send({success:false,error:error});
        } else {
          console.log("Success template");
          res.status(200).send({success: true});
        }
      });
    }
  }
  
};


function isEmpty(str) {
  return (!str || 0 === str.length);
}