
var Message = require('../models/Message');
var CryptoHelper = require('../crypto/CryptoHelper');

var db = require('../db/DBConnections');



exports.index = function(req, res) {
  res.send('NOT IMPLEMENTED: Home Ledger. Index route for testing');
};



// GET
exports.get_messages = function (req,res) {
  let homeId = req.query.home;
  let accountId = req.query.account;
  
  
  Message.find({senderHomeId:homeId,senderAccount:accountId}).then((result) => {
    res.send(result);
    
  }).catch((error) => {
    console.log("Error in getting objects for a home " + error);
  })
  
};

exports.in_messages = function (req,res) {
  let homeId = req.query.home;
  let accountId = req.query.account;
  
  
  Message.find({recipientHomeId:homeId,recipientAccount:accountId}).then((result) => {
    res.send(result);
    
  }).catch((error) => {
    console.log("Error in getting objects for a home " + error);
  })
  
};


exports.sent_messages = function (req,res) {
  let homeId = req.query.home;
  let accountId = req.query.account;
  
  
  Message.find({senderHomeId:homeId,senderAccount:accountId}).then((result) => {
    res.send(result);
    
  }).catch((error) => {
    console.log("Error in getting objects for a home " + error);
  })
  
};


// POST
exports.send_message = function(req, res) {
  
  const tx = req.body;
  
  // header field
  const senderHomeId = tx.header.senderHomeId;
  const senderAccount = tx.header.senderAccount;
  const recipientHomeId = tx.header.recipientHomeId;
  const recipientAccount = tx.header.recipientAccount;
  const senderPublicKey = tx.header.senderPublicKey;
  const senderName = tx.header.senderName;
  const hash = tx.header.hash;
  const signature = tx.header.signature;
  
  
  
  if (isEmpty(hash) || isEmpty(signature) || isEmpty(senderHomeId)
    || isEmpty(senderAccount) || isEmpty(recipientHomeId) || isEmpty(recipientAccount)
  
  ) {
    res.send("Invalid transaction..");
    return;
  }
  
  // TODO: get the sender PK from the home tx. For now trust the incoming tx value
  
  
  // Validate signature
  const result = CryptoHelper.validateSignature(senderPublicKey,hash,signature);
  
  // TODO: Validate other things...
  
  if (!result) {
    res.send({success:false,error:"Signature validation failed..."})
    
  } else {
  
    const message = new Message({_id:hash,senderHomeId:senderHomeId,
      senderAccount:senderAccount,senderName:senderName, recipientHomeId:recipientHomeId,recipientAccount:recipientAccount,
      timeStamp:tx.header.timeStamp,ledgerId:0,body:tx.body});
    
    message.save().then((result) =>  {
      
        return res.send({success:true});
      
    }).catch((error => {
      return res.send({success:false,error:error});
    }))
  }
};


function isEmpty(str) {
  return (!str || 0 === str.length);
}