
var TxPool = require('../db/TxPoolDB');
var LedgerDB = require('../db/HomeLedgerDB');
var HomeLedger = require('../models/HomeLedger');
var HomeCache = require('../models/HomeCache');

var CryptoHelper = require('../crypto/CryptoHelper');

var EC = require('elliptic').ec;

var ec = new EC('secp256k1');


// set these variables at initialization. get the value from DB
var currentBlock = 4;
var currentBlockHash = "";


// The keys are temporary. This will be replaced with production solution...

const secret = "The producer of this network. This seret will stay secret until no more secret...";

const ecurve = new EC('secp256k1');

const producerKeys = ecurve.genKeyPair({entropy:secret});

const producerPublicKey = producerKeys.getPublic().encode('hex');

const producerPrivateKey = producerKeys.getPrivate("hex");


exports.produceBlock = function() {
  
  try {
    
    TxPool.getHomeTxObject().find({ledgerId:0}).limit(20).then((result,error) => {
      
      if (result.length === 0 ) {
        return;
      }
      
      console.log(" Producing home transactions  :"  + result.length);
  
  
      const txSize = JSON.stringify(result);
      
      const hash = CryptoHelper.getHash(txSize);
      
      const sign = CryptoHelper.signIt(producerPrivateKey,hash);
      const newBlockId = currentBlock +1 ;
  
      var block = new HomeLedger({
        _id: newBlockId, hash: hash, previousBlockHash: currentBlockHash,
        blockProducer: "Producer1", blockProducerSign: sign, numberOfTx: txSize.length, txs: txSize
      });
      
      block.save((error, result1) => {
        
        if (result1) {
          
          currentBlock++;
          currentBlockHash = hash;
          
          console.log("Block Produced..."  + currentBlock);
  
          var toUpdate = [];
  
          result.forEach((doc) => {
    
            toUpdate.push(
              { updateOne :
                {
                  "filter": {_id:doc._id},
                  "update": {$set: {ledgerId:currentBlock}}
                }
              }
            )
    
          });
  
          
          TxPool.getHomeTxObject().collection.bulkWrite( toUpdate).then(res => {
            
            console.log("HomeTx in new blocked marked with the blockId");
  
            var cacheUpdate = [];
  
            result.forEach((doc) => {
    
              if (doc.tx.action ==="newHome") {
      
                cacheUpdate.push(
                  { insertOne :
                    {
                      document: {_id:doc.homeId,txs:[{_id:doc.txHash,ledgerId:10,tx:JSON.stringify(doc)}]}
            
                    }
                  }
                )
      
              } else {
      
                cacheUpdate.push(
                  { updateOne :
                    {
                      "filter": {_id:doc.homeId},
                      "update": {$push: {txs:{_id:doc.txHash,ledgerId:10,tx:JSON.stringify(doc)}}}
                    }
                  }
                )
      
              }});
  
            HomeCache.collection.bulkWrite( cacheUpdate).then(res => {
              console.log(res.n);
    
              console.log("Home cache updated...");
            }). catch (error => {
              console.log("Error in updating home cache......");
              console.log(error);
            })
            // set the status to onLedger
          }). catch (error => {
            console.log(error);
          })
          
        } else {
          console.log("Eror in producing block... "  + error);
        }
        
      })
      
    })
    
  } catch (Error) {
    return "Error in producing block";
  }
};

function setCurrentBlockId() {
  
  HomeLedger.find().sort({_id:-1}).limit(1).then((value,error) => {
    if (error) {
      console.log("Error connecting to MongoDB");
    }
    if (value) {
      const id = value[0]._id;
      console.log("Home Producer != Current block number is  " + id);
      currentBlock = id;
    }
    
  });
}

 setCurrentBlockId();
