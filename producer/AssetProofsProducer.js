
let AssetProofLedger = require('../models/ledger/AssetProofLedger');

let AssetProof = require('../models/AssetProof');

let CryptoHelper = require('../crypto/CryptoHelper');

let EC = require('elliptic').ec;

let ec = new EC('secp256k1');


// set these variables at initialization. get the value from DB
let currentBlock = 0;
let currentBlockHash = "";


// The keys are temporary. This will be replaced with production solution...

const secret = "The producer of this network. This secret will stay secret until no more secret...";

const ecurve = new EC('secp256k1');

const producerKeys = ecurve.genKeyPair({entropy:secret});

const producerPublicKey = producerKeys.getPublic().encode('hex');

const producerPrivateKey = producerKeys.getPrivate("hex");


exports.produceBlock = function() {
  
  try {
  
    AssetProof.find({blockId:0}).limit(20).then((result,error) => {
      
      if (result.length === 0 ) {
        return;
      }
      
      console.log(" Asset Ledger: Producing block for Asset Proof with Txs  :"  + result.length);
      
      const txSize = JSON.stringify(result);
      
      const hash = CryptoHelper.getHash(txSize);
      
      const sign = CryptoHelper.signIt(producerPrivateKey,hash);
      const newBlockId = currentBlock +1;
  
      let block = new AssetProofLedger({
        _id: newBlockId, hash: hash, previousBlockHash: currentBlockHash,
        blockProducer: "AssetProofProducer", blockProducerSign: sign, numberOfTx: result.length, txs: txSize
      });
      
      block.save((error, result1) => {
        
        if (result1) {
          
          currentBlock++;
          currentBlockHash = hash;
          
          console.log("Asset Proof Block Produced..."  + currentBlock);
  
          var toUpdate = [];
  
          result.forEach((doc) => {
    
            toUpdate.push(
              { updateOne :
                {
                  "filter": {_id:doc._id},
                  "update": {$set: {blockId:currentBlock}}
                }
              }
            )
    
          });
  
  
          AssetProof.collection.bulkWrite( toUpdate).then(res => {
            console.log("Asset Ledger: AssetProof Txs updated with block Id  " + currentBlock);
          });
          
        } else {
          console.log("Asset Ledger: Error in producing block... "  + error);
        }
        
      })
      
    })
    
  } catch (err) {
    return "Error in producing block at " + err;
  }
};

function setCurrentBlockId() {
  
  AssetProofLedger.find().sort({_id:-1}).limit(1).then((value, error) => {
    
    if (error) return;
    
    if (value) {
  
      if (value.length === 0) {
        return;
      }
      const id = value[0]._id;
  
      if (id > 0)
        currentBlock = id;
  
      console.log("Asset Proof ledger: Current block Id is  " + currentBlock);
    }
  });
  
}

setCurrentBlockId();
