
let TxPool = require('../db/TxPoolDB');

let ObjectLedger = require('../models/ObjectLedger');
let ObjectCache = require('../models/ObjectCache');
let ObjectTxPool = require('../models/ObjectTxPool');

let CryptoHelper = require('../crypto/CryptoHelper');

let EC = require('elliptic').ec;

let ec = new EC('secp256k1');


// set these variables at initialization. get the value from DB
let currentBlock = 0;
let currentBlockHash = "";


// The keys are temporary. This will be replaced with production solution...

const secret = "The producer of this network. This seret will stay secret until no more secret...";

const ecurve = new EC('secp256k1');

const producerKeys = ecurve.genKeyPair({entropy:secret});

const producerPublicKey = producerKeys.getPublic().encode('hex');

const producerPrivateKey = producerKeys.getPrivate("hex");


exports.produceBlock = function() {
  
  try {
  
    ObjectTxPool.find({ledgerId:0}).limit(20).then((result,error) => {
      
      if (result.length === 0 ) {
        return;
      }
      
      console.log(" Producing Object Ledger block with number of transactions... :"  + result.length);
      
      const txSize = JSON.stringify(result);
      
      const hash = CryptoHelper.getHash(txSize);
      
      const sign = CryptoHelper.signIt(producerPrivateKey,hash);
      const newBlockId = currentBlock +1 ;
  
      let block = new ObjectLedger({
        _id: newBlockId, hash: hash, previousBlockHash: currentBlockHash,
        blockProducer: "Producer1", blockProducerSign: sign, numberOfTx: result.length, txs: txSize
      });
      
      block.save((error, result1) => {
        
        if (result1) {
          
          currentBlock++;
          currentBlockHash = hash;
          
          console.log("Block Produced..."  + currentBlock);
  
          var toUpdate = [];
  
          result.forEach((doc) => {
    
            toUpdate.push(
              { updateOne :
                {
                  "filter": {_id:doc._id},
                  "update": {$set: {ledgerId:currentBlock}}
                }
              }
            )
    
          });
  
  
          ObjectTxPool.collection.bulkWrite( toUpdate).then(res => {
            
            console.log("ObjectTxPool marked with the blockId successfully...");
  
            var cacheUpdate = [];
  
            result.forEach((doc) => {
    
              if (doc.tx.action ==="createObject") {
      
                cacheUpdate.push(
                  { insertOne :
                    {
                      document: {_id:doc._id,homeId:doc.homeId,objectId:doc.objectId,nonce:doc.nonce,txs:[{ledgerId:10,tx:doc}]}
                    }
                  }
                )
      
              } else {
      
                cacheUpdate.push(
                  { updateOne :
                    {
                      "filter": {homeId:doc.homeId,objectId:doc.objectId},
                      "update": {$push: {txs:{ledgerId:10,tx:doc}}}
                    }
                  }
                )
      
              }});
            
            ObjectCache.collection.bulkWrite( cacheUpdate).then(res => {
              console.log(res.n);
    
              console.log("Object Cache updated..");
            }).catch (error => {
              console.log("Error in saving object cache..." + error);
              console.log(error);
            })
            
          });
          
        } else {
          console.log("Eror in producing block... "  + error);
        }
        
      })
      
    })
    
  } catch (err) {
    return "Error in producing block at " + err;
  }
};

function setCurrentBlockId() {
  
  ObjectLedger.find().sort({_id:-1}).limit(1).then((value, error) => {
    
    if (error) {
      console.log("Error in connecting to mongodb");
      return;
    }
    
    if (value) {
      if (value.length === 0) {
        return;
      }
      const id = value[0]._id;
      console.log("Object ledger: Current block Id is  " + id);
  
      if (id > 0)
        currentBlock = id;
  
      console.log("Object ledger: Current block Id is  " + currentBlock);
    }
  });
  
}

setCurrentBlockId();
