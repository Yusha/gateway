
let crypto = require('crypto');
let EC = require('elliptic').ec;
let ec = new EC('secp256k1');



exports.validateSignature = function (pub, data,signature) {
  let key = ec.keyFromPublic(pub, 'hex');
  
  return key.verify(data, signature);
  
};


exports.getHash = function(value) {
  
  const hash = crypto.createHash('sha256');
  const toHash = Buffer.from(value,'utf-8');
  hash.update(toHash);
  return hash.digest('hex');
  
};

exports.signIt = function(pk,data) {
  
  const ecSK = ec.keyFromPrivate(pk, 'hex');
  const sign = ecSK.sign(data);
  return sign.toDER('hex');
};
