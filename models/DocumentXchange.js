

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


/**
 * DocumentXchange Schema
 * type as an enum of general, access, accessGranted, accessDenied, others...
 */


const DocumentXchangeSchema = new Schema( {
  _id                 :  String,
  senderHomeId        :  String,
  senderAccount       :  String,
  recipientHomeId     :  String,
  recipientAccount    :  String,
  tx                  :  Schema.Types.Mixed,
});

//Export function to create model class
module.exports = mongoose.model('DocumentXchange', DocumentXchangeSchema );
