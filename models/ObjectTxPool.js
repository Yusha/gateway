

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// The ledger will validate for the objectId existence and the nonce.
// The nonce starts with 1 and goes in increments of 1. Any mismatch will
// result in the rejection of the tx.


var ObjectTxPoolSchema = new Schema( {
  _id                 :  String,
  homeId              :  String,
  objectId            :  String,
  nonce               :  Number,
  user                :  String,
  ledgerId            :  Number,
  tx                  :  Schema.Types.Mixed
});



//Export function to create model class
module.exports = mongoose.model('ObjectTxPool', ObjectTxPoolSchema );
