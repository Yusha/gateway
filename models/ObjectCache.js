
var mongoose = require('mongoose');

var Schema = mongoose.Schema;


var ObjectCacheSchema = new Schema({
  _id             : String,
  homeId          : String,
  objectId        : String,
  Nonce           : Number,
  txs             : [Schema.Types.Mixed]
  
});



//Export function to create "SomeModel" model class
module.exports = mongoose.model('ObjectCache', ObjectCacheSchema );
