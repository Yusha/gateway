

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * HomeTxPool stores all the incoming Home requests. The request are validated before writing to the pool.
 * The validation includes cryptographic signature and hash verifications.
 * The name collision checks and version check.
 * Transaction in the pool does not warranty that it will make to the ledger.
 * This also serve the request for status check.
 * TODO: add a status field in the schema in the future if need warrants it.
 */


const InvitationSchema = new Schema( {
  _id                 :  String,
  timeStamp           :  String,
  action              :  String,
  homeId              :  String,
  invitee             :  String,
  user                :  String,
  group               :  String,
  role                :  String,
  signature           :  String,
  claimed             :  Boolean,
  approved            :  Boolean,
  validated           :  Boolean,
  accountCreated      :  Boolean,
  tx                  :  Schema.Types.Mixed,
  validationTx        :  Schema.Types.Mixed,
  claimedTx           :  Schema.Types.Mixed,
  approvalTx          :  Schema.Types.Mixed,
});


//Export function to create model class
module.exports = mongoose.model('Invitations', InvitationSchema );
