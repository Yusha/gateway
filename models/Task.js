

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


/**
 * A Task is an outstanding job for an account or user
 *
 */


const TaskSchema = new Schema( {
  _id                 :  String,
  action              :  String,
  homeId              :  String,
  accountId           :  String,
  appId               :  String,
  approver            :  String,
  approverHome        :  String,
  appReference        :  String,
  timeStamp           :  String,
  approved            :  Boolean
  
});


//Export function to create model class
module.exports = mongoose.model('Task', TaskSchema );
