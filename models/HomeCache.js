

var mongoose = require('mongoose');
var Schema = mongoose.Schema;



var HomeTx = new Schema( {
  _id:        String,
  ledgerId:   Number,
  tx:         String
  
});


var HomeCacheSchema = new Schema( {
  _id                 :  String,
  txs                 : [HomeTx]
  
});

//Export function to create model class
module.exports = mongoose.model('HomeCache', HomeCacheSchema );