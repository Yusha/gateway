

var mongoose = require('mongoose');
var Schema = mongoose.Schema;



var HomeLedgerSchema = new Schema( {
  _id                 : Number,
  blockId             : Number,
  hash                : String,
  previousBlockHash   : String,
  blockProducer       : String,
  blockProducerSign   : String,
  numberOfTx          : Number,
  txs             : [Schema.Types.Mixed]
});



//Export function to create model class
module.exports = mongoose.model('HomeLedger', HomeLedgerSchema );