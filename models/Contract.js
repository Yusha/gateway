

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


/**
 * Contract Schema
 * A Contract Schema. Identifier by an account.
 * Contract tx might be encrypted or clear text.
 * The header / top level fields are always clear text
 *
 */


const ContractSchema = new Schema( {
  _id                 :  String, // Hash of the Contract
  originator          :  String,
  header              :  Schema.Types.Mixed,
  tx                  :  Schema.Types.Mixed,
  txe                 : [Schema.Types.Mixed], // encrypted contract
  signatures          : [Schema.Types.Mixed]
  
});

module.exports = mongoose.model('Contract', ContractSchema );
