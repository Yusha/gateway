

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var AddressSchema = new Schema( {
  _id       :   Number,
  address1  :   String,
  address2  :   String,
  state     :   String,
  Country   :   String,
  Zip       :   String,
  status    :   String
  
});

var AdminAccountSchema = new Schema( {
  name      :   String,
  email     :   String,
  publicKey :   String,
  status    :   String,
  blockId   :   Number
  
});

var AccountSchema = new Schema( {
  _id       :   Number,
  name      :   String,
  email     :   String,
  publicKey :   String,
  blockId   :   Number,
  status    :   String
  
  
});

var PhoneSchema = new Schema( {
  _id         :   Number,
  number      :   String,
  name        :   String,
  status      :   String,
  verified    :   Boolean,
  blockId     :   Number
  
});

var EmailSchema = new Schema( {
  _id          :  String,
  email        :  String,
  status       :  String,
  verified     :  Boolean,
  blockId      :  Number
});



var HomeCollectionSchema = new Schema({
  _id             : String,
  homeId          : String,
  blockId         : Number,
  phones          : [PhoneSchema],
  emails          : [EmailSchema],
  addresses       : [AddressSchema],
  adminAccounts   : [AdminAccountSchema],
  accounts        : [AccountSchema]
});



//Export function to create "SomeModel" model class
module.exports = mongoose.model('HomeCollectionModel', HomeCollectionSchema );


