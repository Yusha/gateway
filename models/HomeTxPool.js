

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * HomeTxPool stores all the incoming Home requests. The request are validated before writing to the pool.
 * The validation includes cryptographic signature and hash verifications.
 * The name collision checks and version check.
 * Transaction in the pool does not warranty that it will make to the ledger.
 * This also serve the request for status check.
 * TODO: add a status field in the schema in the future if need warrants it.
 */


var HomeTxPoolSchema = new Schema( {
  _id                 :  String,
  homeId              :  String,
  ledgerId            :  Number,
  txHash              :  String,
  prvHash             :  String,
  
  tx                  :  Schema.Types.Mixed,
});

//Export function to create model class
module.exports = mongoose.model('HomeTxPoolModel', HomeTxPoolSchema );
