

var mongoose = require('mongoose');
var Schema = mongoose.Schema;



var AssetProofBlockSchema = new Schema( {
  _id                 : String,
  blockId             : Number,
  Hash                : String,
  previousBlockHash   : String,
  blockProducer       : String,
  blockProducerSign   : String,
  numberOfTx          : Number,
  txs                 : [String]
});


//Export function to create model class
module.exports = mongoose.model('AssetProofLedger', AssetProofBlockSchema );