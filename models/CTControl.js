

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


/**
 * Contract Template Control Schema
 
 */


const CTControlSchema = new Schema( {
  _id                 :  String, // Hash of the template
  publisher           :  String,
  tx                  :  Schema.Types.Mixed,
  
});

//Export function to create model class
module.exports = mongoose.model('TemplateControl', CTControlSchema );
