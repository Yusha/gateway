

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


/**
 * Message Schema
 * type as an enum of general, access, accessGranted, accessDenied, others...
 */


const SharedObjectsSchema = new Schema( {
  _id                 :  String,
  timeStamp           :  String,
  ledgerId            :  Number,
  tx                  :  Schema.Types.Mixed,
});

//Export function to create model class
module.exports = mongoose.model('SharedObject', SharedObjectsSchema );
