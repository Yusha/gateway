

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


/**
 * A signature task for a contract
 *
 */


const TaskSchema = new Schema( {
  _id                 :  String,
  homeId              :  String,
  accountId           :  String,
  contractId          :  String,
  name                :  String,
  timeStamp           :  String,
  completed           :  Boolean
});


//Export function to create model class
module.exports = mongoose.model('SignTask', TaskSchema );
