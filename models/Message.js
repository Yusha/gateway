

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


/**
 * Message Schema
 * type as an enum of general, access, accessGranted, accessDenied, others...
 */


const MessageSchema = new Schema( {
  _id                 :  String,
  senderHomeId        :  String,
  senderAccount       :  String,
  senderName          :  String,
  recipientHomeId     :  String,
  recipientAccount    :  String,
  type                :  String,
  timeStamp           :  String,
  ledgerId            :  Number,
  body                :  Schema.Types.Mixed,
});

//Export function to create model class
module.exports = mongoose.model('Message', MessageSchema );
