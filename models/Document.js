

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


/**
 * Document Schema
 * A document is owned by an account.
 */


const DocumentSchema = new Schema( {
  _id                 :  String, // Name of the document
  ledgerId            :  Number,
  tx                  :  Schema.Types.Mixed,
  
});

//Export function to create model class
module.exports = mongoose.model('Document', DocumentSchema );
