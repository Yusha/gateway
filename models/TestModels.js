
let mongoose = require('mongoose');
let homeTx = require("./HomeTxPool");
let homeLedger = require("./HomeLedger");
let homeCollection = require ("./HomesCollectionModel");
let homeCache = require ("./HomeCache");

let ObjectTxPool = require("./ObjectTxPool");
let ObjectLedger = require("./ObjectLedger");
let ObjectCache = require("./ObjectCache");


let mongoDB = 'mongodb://127.0.0.1/home';

mongoose.connect(mongoDB, { useNewUrlParser: true });
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));


// let testHomeTx = new homeTx({_id:"myhome2", homeId:"MyHome1",hash:"oxedd", tx:"The real tx" });
//let home1 = new homeCollection({_id:"testHome2",homeId:"myhome1",blockId:1,phones:[{_id:1,number:9783945425,verified:true,status:"active"}]});

/**
testHomeTx.save(function(error,home) {
  if (error) {
    console.log("Error received  " + error);
    
  } else {
    console.log("Saved ..." + JSON.stringify(home));
    
  }
  
});
 


testHomeTx.save().then((object) => {
  console.log("Saved.... " + JSON.stringify(object));
  return home1.save();
  

  }).then((object1) => {
    console.log("Saved.... " + JSON.stringify(object1));
  
  }).
    catch((error) => {
    console.log("Got error " + error);

  });

 */

function testfunction () {
  
  
  
  const all = homeTx.find({}).limit(20).then((result, error) => {
    
    /**
  
    var toUpdate = [{ updateOne :
      {
        "filter": {_id:"cd618bab963ef66c36a3fb61f65540620ad8e455cc4d5299f057fb6411948a20"},
        "update": {ledgerId:20}
      }
    },
      { updateOne :
        {
          "filter": {_id:"d090aa29156166ce8df113da7622a1ceafc4e5ae55d5d5e8c7431bc1bcfadbc6"},
          "update": {ledgerId:10}
        }
      },
    
    
    ]
     */
    
    var toUpdate = [];
    
    result.forEach((doc) => {
  
      toUpdate.push(
        { updateOne :
          {
            "filter": {_id:doc._id},
            "update": {$set: {ledgerId:0}}
          }
        }
      )
      
    });
    
    
    
    
    homeTx.collection.bulkWrite( toUpdate).then(res => {
      console.log(res.n);
      console.log(res.modified);
      console.log("Worked");
    }). catch (error => {
      console.log(error);
    })
    
    
    
    
  });
  
}

function testHomeCache() {
  
  const all = homeTx.find({}).limit(20).then((result, error) => {
    
    var cacheUpdate = [];
    
    result.forEach((doc) => {
      
      if (doc.tx.action ==="newHome") {
  
        cacheUpdate.push(
          { insertOne :
            {
              document: {_id:doc.homeId,txs:[{_id:doc.txHash,ledgerId:10,tx:JSON.stringify(doc)}]}
             
            }
          }
        )
        
      } else {
  
        cacheUpdate.push(
          { updateOne :
            {
              "filter": {_id:doc.homeId},
              "update": {$push: {txs:{_id:doc.txHash,ledgerId:10,tx:JSON.stringify(doc)}}}
            }
          }
        )
      
      }});
    
    
    
    
    homeCache.collection.bulkWrite( cacheUpdate).then(res => {
      console.log(res.n);
      
      console.log("Worked");
    }). catch (error => {
      console.log("Error buddy... keep debugging");
      console.log(error);
    })
    
    
    
    
  });
  
  
}

function testObjectCache() {
  
  const all = ObjectTxPool.find({}).limit(20).then((result, error) => {
    
    if (result.length === 0) {
      console.log("Nothing to process");
      return;
    }
    
    var cacheUpdate = [];
    
    result.forEach((doc) => {
      
      if (doc.tx.action ==="createObject") {
        
        cacheUpdate.push(
          { insertOne :
            {
              document: {_id:doc._id,homeId:doc.homeId,objectId:doc.objectId,nonce:doc.nonce,txs:[{ledgerId:10,tx:doc}]}
              
            }
          }
        )
        
      } else {
        
        cacheUpdate.push(
          { updateOne :
            {
              "filter": {homeId:doc.homeId,objectId:doc.objectId},
              "update": {$push: {txs:{ledgerId:10,tx:doc}}}
            }
          }
        )
        
      }});
  
  
  
  
    ObjectCache.collection.bulkWrite( cacheUpdate).then(res => {
      console.log(res.n);
      
      console.log("Worked");
    }). catch (error => {
      console.log("Error buddy... keep debugging");
      console.log(error);
    })
    
    
    
    
  });
  
  
}

// testfunction();
// testHomeCache();
// testObjectCache();

const get_groups = function() {
  
  
  
  homeTx.find({homeId:"Microsoft",'tx.action':"createRole"}).then(result => {
    console.log(JSON.stringify(result));
  })
};

// get_groups();

const get_account = function () {
  homeTx.find({homeId:"Microsoft",'tx.action':"newAccount",'tx.owner':"akbar3@microsoft.com"}).then(result => {
    JSON.stringify(result);
  })
  
};

// get_account();

const homeURL = "Microsoft/akbar.ansari@gmail.com";
const index = homeURL.indexOf("/");
const homeId = homeURL.substring(0,index);
const account = homeURL.substring(index+1,homeURL.length);

console.log("homeId  " + homeId);
console.log("Account  " + account);


var timestamp = Number(new Date())

var date = new Date(timestamp).toDateString();
console.log(date);














