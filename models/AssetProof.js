

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


/**
 * Asset proofs to serve FakeProof
 *
 */


const AssetProofSchema = new Schema( {
  _id                 :  String,
  homeId              :  String,
  blockId             :  Number,
  tx                  :  Schema.Types.Mixed,
});

//Export function to create model class
module.exports = mongoose.model('AssetProof', AssetProofSchema );
