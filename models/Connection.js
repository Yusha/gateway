

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


/**
 * Connection Schema
 */


const ConnectionSchema = new Schema( {
  _id                 :  String,
  timeStamp           :  String,
  ledgerId            :  Number,
  tx                  :  Schema.Types.Mixed,
});

//Export function to create model class
module.exports = mongoose.model('Connection', ConnectionSchema );
