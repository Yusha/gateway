

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// The ledger will validate for the objectId existence and the nonce.
// The nonce starts with 1 and goes in increments of 1. Any mismatch will
// result in the rejection of the tx.


var PISchema = new Schema( {
  _id                 : String,
  blockId             : Number,
  tx                  : Schema.Types.Mixed,
  approvingTx         : Schema.Types.Mixed,
  retireTx            : Schema.Types.Mixed,
  retireBlockId       : Number,
});

//Export function to create model class
module.exports = mongoose.model('PI', PISchema );