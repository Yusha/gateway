

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


/**
 * Contract Schema
 * A Contract Schema. Identifier by an account.
 * Contract tx might be encrypted or clear text.
 * The header / top level fields are always clear text
 *
 */


const DocumentContractSchema = new Schema( {
  _id                 :  String, // Hash of the Contract
  originator          :  String,
  header              :  Schema.Types.Mixed,
  tx                  :  Schema.Types.Mixed,
  
});

//Export function to create model class
module.exports = mongoose.model('DocumentContract', DocumentContractSchema );
