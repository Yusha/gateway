

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


/**
 * Contract Template Schema
 * A Contract Template Schema. Identifier by an account.
 * System Templates are public and visible to everyone
 * Home Templates may be encrypted yet visible to everyone
 */


const CTemplateSchema = new Schema( {
  _id                 :  String, // Hash of the template
  publisher           :  String,
  tx                  :  Schema.Types.Mixed,
  
});

//Export function to create model class
module.exports = mongoose.model('CTemplate', CTemplateSchema );
