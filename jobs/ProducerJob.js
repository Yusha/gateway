
const cron = require('node-cron');
const homeProducer = require('../producer/HomeProducer');
const objectProducer = require('../producer/ObjectProducer');
const assetProofProducer = require('../producer/AssetProofsProducer');

// Producer ledger blocks every minute. No block is produced if there is nothing in the queue.

exports.startProducers = function() {
  
  console.log('running Producer Task every minute...');
  
  cron.schedule('* * * * *', () => {
    homeProducer.produceBlock();
    objectProducer.produceBlock();
    assetProofProducer.produceBlock();
  });
};